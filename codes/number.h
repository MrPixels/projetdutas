#include <stdio.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>

#define X 630                       //Largeur de la fenetre
#define Y 480                       //Hauteur de la fenetre
#define EPAISSEUR_TRONC 10          //Largeur du trait central
#define HAUTEUR_TRONC (Y * 0.66)    //hauteur du trait central

//permet d'empecher la fenetre de se fermer immediatement
void pause();

//initialise la SDL + charge la base de la numeration forestiere
void init();

//ajoute une centaine sur le resultat
void centaines();

//ajoute une dizaine sur le resultat
void dizaines();

//ajoute cinq au resultat
void cinquaine();

//ajoute une unite au resultat
void unites();

#include "number.h"

SDL_Surface *screen; //Variable globale écran appelée de nombreuses fois

void pause()
{
	int continuer = 1;
	SDL_Event event;

	while (continuer)
	{
		SDL_WaitEvent(&event);
		switch(event.type)
		{
			case(SDL_QUIT):
				continuer = 0;
		}
	}
}

void init()
{
	SDL_Surface *tronc;
	SDL_Rect positionTronc;
	SDL_Surface *background;
	SDL_Rect positionBackground;

	positionTronc.x = (X / 2) - (EPAISSEUR_TRONC / 2); //Tronc placé au centre
	positionTronc.y = (Y / 2) - ((HAUTEUR_TRONC) / 2);
	positionBackground.x = 0;
	positionBackground.y = 0;

	SDL_Init(SDL_INIT_VIDEO);
	screen = SDL_SetVideoMode(X, Y, 32, SDL_HWSURFACE);
	tronc = SDL_CreateRGBSurface(SDL_HWSURFACE, EPAISSEUR_TRONC, HAUTEUR_TRONC, 32, 0, 0, 0, 0);
	background = IMG_Load("../Images/bg.png");

  if(!background){
    printf("Error while loading: %s\n", IMG_GetError());
    exit(0);
  }

	SDL_WM_SetCaption("Numération forestière", NULL);
	SDL_FillRect(tronc , NULL, SDL_MapRGB(screen->format, 255, 177, 76)); //Couleur du tronc en noir
	SDL_BlitSurface(background, NULL, screen, &positionBackground);
	SDL_BlitSurface(tronc, NULL, screen, &positionTronc);
	
  SDL_Flip(screen);
}

void centaines(int nb)
{
	SDL_Surface *cent;
	SDL_Rect positionCent;
	int sens;
	sens = 1;
	if (nb % 2 == 0) 
		sens = -1;
	positionCent.y = 20;
	positionCent.x = (X / 2) + (0.04 * nb * X * sens);
	cent = IMG_Load("../Images/cent.png");
  if(!cent){
    printf("Error while loading: %s\n", IMG_GetError());
    exit(0);
  }
	SDL_BlitSurface(cent, NULL, screen, &positionCent);
	SDL_Flip(screen);
}

void dizaines(int nb)
{
	SDL_Surface *dix;
	SDL_Rect positionDix;

	positionDix.x = (X / 2) - (237 / 2);
	positionDix.y = (0.15 * Y) + ((0.05) * nb * Y); //décale la barre vers le bas en fonction du nb d'appels de la fonction
	dix = IMG_Load("../Images/dix.png");
	if(!dix){
    printf("Error while loading: %s\n", IMG_GetError());
  }
  SDL_BlitSurface(dix, NULL, screen, &positionDix);
	SDL_Flip(screen);
}

void cinquaine(int nb) //Pas d'arguments car une seule barre
{
	SDL_Surface *cinq;
	SDL_Rect positionCinq;

	positionCinq.x = (X / 2) - (237 / 2);
	positionCinq.y = (0.15 * Y) + ((0.05) * nb * Y); //décale la barre vers le bas en fonction du nb d'appels de la fonction
	cinq = IMG_Load("../Images/cinq.png");
  if(!cinq){
    printf("Error while loading: %s\n", IMG_GetError());
  }
	SDL_BlitSurface(cinq, NULL, screen, &positionCinq);
	SDL_Flip(screen);
}

void unites(int nb)
{
	SDL_Surface *un;
	SDL_Rect positionUn;

	switch (nb) //Que 4 possibilités pour les unités
	{
		case 1:
			positionUn.x = X - 180;
			positionUn.y = Y - 66;
			break;
		case 2:
			positionUn.x = X - 300;
			positionUn.y = Y - 66;
			break;
		case 3:
			positionUn.x = X - 180;
			positionUn.y = Y - 110;
			break;
		case 4:
			positionUn.x = X - 300;
			positionUn.y = Y - 110;
			break;

	}
	un = IMG_Load("../Images/un.png");
  if(!un){
    printf("Error while loading: %s\n", IMG_GetError());
  }
	SDL_BlitSurface(un, NULL, screen, &positionUn);
	SDL_Flip(screen);
}

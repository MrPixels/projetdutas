#include"proj1.h"

int main(){
	numerationForestiere();
	return 0;
}

void numerationForestiere(){
	int nb;
	do{
		printf("nombre à traduire ?\n");
		scanf("%d", &nb);
		if(nb>999){
			printf("%d est trop grand pour une traduction forestière.\nMerci de choisir un nombre entre 0 et 999\n\n", nb);
		}
		else if(nb<0){
			printf("%d est trop petit pour une traduction forestière.\nMerci de choisir un nombre entre 0 et 999\n\n", nb);
		}
	}while(nb<0 || nb>999);
	numerationF(nb);
}

void numerationF(int nb){
	int i; //compteur égal au nombre de fois qu'une fonction est appelée

	i = 0;
	init();
	while(nb>=100){
		centaines(++i);
		nb -= 100;
	}
	i = 0;
	while(nb>=10){
		dizaines(++i);
		nb -= 10;
	}
	i ++;
	while(nb>=5){
		cinquaine(++i);
		nb -= 5;
	}
	i = 0;
	while(nb>=1){
		unites(++i);
		nb -= 1;
	}
	pause();
}
		  
